package MiniProject;

import java.util.Date;
import java.util.List;

public class Bills {
	private String username;
	private List<MenuItems> items;
	private double cost;
	private Date time;
	
	public Bills() {}
	
	public Bills(String name, List<MenuItems> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.username = username;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getusername() {
		return username;
	}
	public void setusername(String name) {
		this.username = username;
	}
	
	public List<MenuItems> getItems() {
		return items;
	}
	public void setItems(List<MenuItems> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}

}
